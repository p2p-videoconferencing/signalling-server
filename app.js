require('dotenv/config');
const express = require('express')
const http = require('http');
const socketServer = require('./socket/socketServer.js');
const config = require('./infrastructure/config.js')();
const prometheusMiddleware = require('express-prometheus-middleware');
const logger = require('./infrastructure/request-logger.js')
const app = express();
logger(app);

app.get('/health', (req, res) => {
    res.status(200)
    res.json()
});

const appConfig = config.app;
const server = http.createServer(app);
socketServer(server);
app.use(prometheusMiddleware({
    metricsPath: '/ws',
    collectDefaultMetrics: true,
    requestDurationBuckets: [0.1, 0.5, 1, 1.5],
    requestLengthBuckets: [512, 1024, 5120, 10240, 51200, 102400],
    responseLengthBuckets: [512, 1024, 5120, 10240, 51200, 102400],
    prefix: 'p2p_signalling_server_',
}));
const port = parseInt(appConfig.port) || 80;
server.listen(port, () => console.log(`Server is running on port ${port}`));
