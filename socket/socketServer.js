const socket = require('socket.io')
const {getRoom, getSocketRoom, setRoom, setSocketRoom, initClient} = require("../infrastructure/redis");

const serverConfig = {
    path: '/ws',
    cors: {
    origin: '*'
}}

module.exports = async (server) => {
    await initClient();
    const io = socket(server, serverConfig);
    io.on("connection", async (socket) => {
        console.log(`socket with id ${socket.id} connected!`);
        socket.on("join room", async (connectionInfo) => {
            const room = await getRoom(connectionInfo.roomID);
            if (room) {
                const length = room.length;
                if (length === 4) {
                    socket.emit("room full");
                    return;
                }
                room.push({
                    socketID: socket.id,
                    username: connectionInfo.username,
                });
                await setRoom(connectionInfo.roomID, room);
            } else {
                const newRoom = [
                    {
                        socketID: socket.id,
                        username: connectionInfo.username,
                    },
                ];
                await setRoom(connectionInfo.roomID, newRoom);
            }
            await setSocketRoom(socket.id, connectionInfo.roomID);
            const usersInThisRoom = room ? room.filter(
                (user) => user.socketID !== socket.id
            ) : [];
            socket.emit("all users", usersInThisRoom);
        });

        socket.on("sending signal", async (payload) => {
            const room = await getRoom(payload.roomID);
            const user = room.find(
                (user) => user.socketID === payload.callerID
            );
            const userInfo = {
                signal: payload.signal,
                callerID: payload.callerID,
                username: user.username,
            };
            io.to(payload.userToSignal).emit("user joined", userInfo);
        });

        socket.on("returning signal", (payload) => {
            io.to(payload.callerID).emit("receiving returned signal", {
                signal: payload.signal,
                id: socket.id,
            });
        });

        socket.on("disconnect", async () => {
            const roomID = await getSocketRoom(socket.id);
            let room = await getRoom(roomID);
            console.log(room);
            if (room) {
                room = room.filter((peer) => peer.socketID !== socket.id);
                await setRoom(roomID, room);
            }
            socket.broadcast.emit("user left", socket.id);
        });
    });
};