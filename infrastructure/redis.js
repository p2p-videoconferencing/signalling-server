const redis = require('redis')
const config = require('./config.js')();

const ROOM_KEY_PREFIX = 'room_'
const SOCKET_KEY_PREFIX = 'socket_'
const EXPIRE_TIMOUT = 60 * 60 * 24;
const client = redis.createClient(
    { url: config.redis.url }
);

const getRoomKey = (roomId) => {
    return ROOM_KEY_PREFIX + roomId;
}

const getSocketKey = (socketId) => {
    return SOCKET_KEY_PREFIX + socketId;
}

exports.getRoom = async (roomId) => {
    return JSON.parse(await client.get(getRoomKey(roomId)))
}

exports.getSocketRoom = async (socketId) => {
    return JSON.parse(await client.get(getSocketKey(socketId)))
}

exports.setRoom = async (roomId, roomData) => {
    await client.set(getRoomKey(roomId), JSON.stringify(roomData))
    await client.expire(getRoomKey(roomId), EXPIRE_TIMOUT)
}

exports.setSocketRoom = async (socketId, roomId) => {
    await client.set(getSocketKey(socketId), JSON.stringify(roomId))
    await client.expire(getSocketKey(socketId), EXPIRE_TIMOUT)
}

exports.initClient = async () => {
    client.on('error', (err) => console.log('Redis Client Error', err));
    await client.connect();
}