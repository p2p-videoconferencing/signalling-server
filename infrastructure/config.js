module.exports = () => {
  return {
    app: {
      port: process.env.APP_PORT,
    },
    redis: {
      url: process.env.REDIS_URL,
    }
  }};
